
$(document).ready(function() {




  $('.smartphones .owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 20
      }
    }
  });


   $('.slider .owl-carousel').owlCarousel({
    animateOut: 'fadeOut',
    loop: true,
    dot: true,
    margin: 0,
    nav: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        loop: true,
        nav: false
      },
      600: {
        items: 1,
        nav: false,
        loop: true
      },
      1000: {
        items: 1,
        nav: false,
        loop: true,
        margin: 0
      }
    }
  });


  $(function(){
        //#gallery
        $('#gallery')
            .supergallery({
                animation:{
                    type:'slide'
                },
                other:{
                    loop:false,
                    changePageEvent:'mouseenter'
                }
            })
            .on('pageChangeStart',function(e,num){
                //console.log(e.type,num);
                })
            .on('pageChangeEnd',function(e,num){
                //console.log(e.type,num);
            });


        //#gallery2
        var gallery2 = $.superThumbGallery('#gallery2');
            
    });

})

    
