<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'MainController@index');
Route::get('smartphones', 'SmartphoneController@index');
Route::get('/phones', 'PhonesController@index');
Route::get('/gadgets', 'GadgetsController@index');
Route::get('/accessories', 'AccessoriesController@index');

Route::get('/smartphone/result', 'FilterController@index');
Route::get('/phones/result', 'FilterController@phone');
Route::get('/gadgets/result', 'FilterController@gadget');
Route::get('/accessories/result', 'FilterController@accessory');


Route::get('/smartphone/{id?}', 'SingleController@index')->name('id', '[\w\d\-\_]+');
Route::get('/phones/{id?}', 'SinglePhoneController@index')->name('id', '[\w\d\-\_]+');
Route::get('/gadgets/{id?}', 'SingleGadgetController@index')->name('id', '[\w\d\-\_]+');
Route::get('/accessories/{id?}', 'SingleAccessoryController@index')->name('id', '[\w\d\-\_]+');

Route::post('/smartphone/sendmail', 'SingleController@send');
Route::post('/phones/sendmail', 'SinglePhoneController@send');
Route::post('/gadgets/sendmail', 'SingleGadgetController@send');
Route::post('/accessories/sendmail', 'SingleAccessoryController@send');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function() {
Route::resource('admin', 'Admin\\SlidesController');
Route::resource('admin/brands', 'Admin\\BrandsController');
Route::resource('admin/slides', 'Admin\\SlidesController');
Route::resource('admin/smartphones', 'Admin\\SmartphonesController');
Route::resource('admin/accessories', 'Admin\\AccessoriesController');
Route::resource('admin/gadgets', 'Admin\\GadgetsController');
Route::resource('admin/phones', 'Admin\\PhonesController');
});