<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ (isset($phone->title)) ? $phone->title:''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('price') ? 'has-error' : ''}}">
    <label for="price" class="control-label">{{ 'Price' }}</label>
    <input class="form-control" name="price" type="text" id="price" value="{{ (isset($phone->price)) ? $phone->price:''}}" >
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    <label for="content" class="control-label">{{ 'Content' }}</label>
    <textarea class="form-control" rows="5" name="content" type="textarea" id="content" >{{ (isset($phone->content)) ? $phone->content:''}}</textarea>
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('brand') ? 'has-error' : ''}}">
    <label for="brand" class="control-label">{{ 'Brand' }}</label>
    <select name="brand" class="form-control" id="brand" >
    @foreach ( $brands as $brand )
        <option value="{{ $brand->title }}" {{ (isset($smartphone->brand) && $smartphone->brand == $brand->title) ? 'selected' : ''}}>{{ $brand->title }}</option>
    @endforeach
</select>
    {!! $errors->first('brand', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image1') ? 'has-error' : ''}}">
    <label for="image1" class="control-label">{{ 'Image1' }}</label>
    <input class="form-control" name="image1" type="file" id="image1" value="{{ (isset($phone->image1)) ? $phone->image1:''}}" >
    {!! $errors->first('image1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image2') ? 'has-error' : ''}}">
    <label for="image2" class="control-label">{{ 'Image2' }}</label>
    <input class="form-control" name="image2" type="file" id="image2" value="{{ (isset($phone->image2)) ? $phone->image2:''}}" >
    {!! $errors->first('image2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image3') ? 'has-error' : ''}}">
    <label for="image3" class="control-label">{{ 'Image3' }}</label>
    <input class="form-control" name="image3" type="file" id="image3" value="{{ (isset($phone->image3)) ? $phone->image3:''}}" >
    {!! $errors->first('image3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image4') ? 'has-error' : ''}}">
    <label for="image4" class="control-label">{{ 'Image4' }}</label>
    <input class="form-control" name="image4" type="file" id="image4" value="{{ (isset($phone->image4)) ? $phone->image4:''}}" >
    {!! $errors->first('image4', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('have') ? 'has-error' : ''}}">
    <label for="have" class="control-label">{{ 'В наличии' }}</label>
    <select name="have" class="form-control" id="have" >
    @foreach (json_decode('{"В наличии": "В наличии", "На заказ": "На заказ"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($phone->have) && $phone->have == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('have', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
