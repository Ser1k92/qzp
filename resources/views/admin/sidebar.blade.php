<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/slides') }}">
                        Слайды
                    </a>
                </li>
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/smartphones') }}">
                        Смартфоны
                    </a>
                </li>
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/phones') }}">
                        Телефоны
                    </a>
                </li>
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/accessories') }}">
                        Аксессуары
                    </a>
                </li>
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/gadgets') }}">
                        Гаджеты
                    </a>
                </li>
                <li role="presentation" style="width: 100%;margin-bottom: 10px;">
                    <a href="{{ url('/admin/brands') }}">
                        Бренды
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
