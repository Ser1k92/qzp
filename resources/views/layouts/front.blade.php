<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1000">
		<meta name="keywords" content="товары, Qzp Магазин, купить, электроника, покупка" />
		<meta name="description" content="Qzp Магазин — покупка электроники. Удобная процедура оформления, простой процесс покупки и большой ассортимент товаров. Описания товаров, отзывы и лучшие цены в Казахстане." />
		<meta name="robots" content="index, follow" />
		<meta name="HandheldFriendly" content="True" />
		<meta name="format-detection" content="telephone=no">
    <meta property="og:locale" content="ru_KZ" />
	<meta property="og:title" content="Qzp Магазин в Алматы - интернет-магазин электроники" />
    <meta property="og:description" content="Qzp Магазин — покупка бытовой техники и электроники в кредит. Удобная процедура оформления, простой процесс покупки и большой ассортимент товаров. Описания товаров, отзывы и лучшие цены в Казахстане." />
	<meta property="og:url" content="https://qzp.kz/" />
	<meta property="og:image" content="https://qzp.kz/logo_small.png" />
	<meta property="og:site_name" content="Qzp Магазин" />
    <link rel="image_src" type="image/jpeg" href="https://qzp.kz/logo_small.png" />
	<meta name="title" content="Qzp Магазин в Алматы - интернет-магазин электроники и бытовой техники в кредит">
	<link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic" rel="stylesheet">

	<link async rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">


	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Owl Stylesheets -->
    <link rel="stylesheet" href="/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/owlcarousel/assets/owl.theme.default.min.css">


    <link rel="stylesheet" href="/css/animate.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body class="page-homepage pageType-ContentPage template-pages-layout-landingLayout2Page pageLabel-homepage language-ru">
	<header class="header">
                <div class="header_inner">
                    <div class="header_logo">
                        <a href="/">
                            <img src="/images/logo.png" style="height: 39px;">
                        </a>
                    </div>
                    <div class="header_phone "><a href="tel:87074791315" class="headerAuth__link">87074791315</a></div>
                    <ul class="header_menu">
                        <li class="header_menu__item header_menu__item--active">
                                <a href="/" class="header_menu__link">Главная</a>
                            </li>
                        <li class="header_menu__item">
                                <a href="/smartphones" class="header_menu__link">Смартфоны</a>
                            </li>
                        <li class="header_menu__item">
                                <a href="/accessories" class="header_menu__link">Аксессуары</a>
                            </li>
                        <li class="header_menu__item">
                                <a href="/gadgets" class="header_menu__link">Гаджеты</a>
                            </li>
                        <li class="header_menu__item">
                                <a href="/phones" class="header_menu__link">Телефоны</a>
                            </li>
                        </ul>

                    
                </div>
            </header>


@yield('content')



<footer>
	<div class="content">
		<div class="footer-left">
			<h4 class="footer__section-title">Мы в социальных сетях</h4>
			<div class="footer__section-socials">
				<a class="footer__section-socials-link" href="https://www.facebook.com/"><span class="icon _medium _facebook"></span></a>
				<a class="footer__section-socials-link" href="https://vk.com/"><span class="icon _medium _vk"></span></a>
				<a class="footer__section-socials-link" href="https://twitter.com/"><span class="icon _medium _twitter"></span></a>
				<a class="footer__section-socials-link" href="https://www.instagram.com/"><span class="icon _medium _instagram"></span></a>
				<a class="footer__section-socials-link" href="http://my.mail.ru/community/kaspibank"><span class="icon _medium _mailru"></span></a>
				<a class="footer__section-socials-link" href="https://ok.ru/"><span class="icon _medium _odnoklassniki"></span></a>
			</div>
		</div>
		<div class="footer-right">
			<h4 class="footer__section-title">Помощь и контакты</h4>
			<div class="phone ">
						<span class="phone__icon icon _small _mobile-toggle"></span>
						<span class="phone__number" itemprop="telephone"><a href="tel:87074791315" class="headerAuth__link">87074791315</a></span>
					</div>
					<div class="phone ">
						<span class="phone__number" itemprop="telephone" style="font-size: 16px; margin-top:10px;">ТРЦ "ЦУМ", Сектор №9</span>
					</div>
		</div>
	</div>
	<div style="clear:both;"></div>
	<div class="content" style="margin-top: 20px;">
		<div class="footer__copyright">
				<meta itemprop="name" content="Kaspi Магазин">
				<span>© ТОО «Магазин», 2019</span>
			</div>
	</div>
	
</footer>


<script src="/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/assets/owlcarousel/owl.carousel.js"></script>



<script type="text/javascript" src="/js/jquery-supergallery-plugin2.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136855622-1"></script>
<script src="/js/scripts.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136855622-1');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(52965034, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52965034" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>