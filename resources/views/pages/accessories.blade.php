@extends('layouts.front')
@section('content')

<section class="slider">
  <div class="owl-carousel">
            @foreach ($slides as $item)
            <div class="item">
              <a href="{!! $item->url !!}">
                <img src="/storage/{!! $item->image !!}" alt="{!! $item->title !!}" class="">
              </a>   
            </div>
            @endforeach
        </div>
</section>
<section class="smartphones">
	<div class="content">
        
        <div class="filter">
            <form action="{{ url('/accessories/result') }}">

                <p class="title-filters">Фильтр товаров</p>
                <p class="filters__filter-title">Цена</p>
                <div class="search-element-wrap search-element-price">
                    <input type="text" size="6" placeholder="От" name="min_price" value="{!! $min_price !!}"> —
                    <input type="text" size="6" placeholder="До" name="max_price" value="{!! $max_price !!}">
                </div>
                <p class="filters__filter-title">Бренд</p>
                @foreach ($brands as $item)
                <div class="form-filter">
                    <input type="checkbox" id="scales" name="brands[]" value="{{ $item->title }}">
                    <label for="scales">{{ $item->title }}</label>
                </div>
                @endforeach
                
                <button type="submit" class="button item__buy-button">Фильтр</button>
            </form>
         
        </div>
        <div class="products">
            @foreach($accessories as $item)
            <div class="item item-card" style="margin-bottom: 30px;">
                <div class="img-product-wrapper">
                    <div class="item-card__sticker"></div>
                    <a href="/accessories/{!! $item->id !!}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
                        <img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
                    </a>
                    <div class="item-card__info">
                        <div class="item-card__name">
                            <a href="/accessories/{!! $item->id !!}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
                        </div>
                        <div class="item-card__prices">
                            <div class="item-card__debet">
                                <span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
                            </div>
                            <div class="item-card__instalment">
                                <div class="item-card__prices-title">{!! $item->have !!}</div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
            
        
</section>





@endsection