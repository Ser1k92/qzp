@foreach($smartphones as $item)
	<div class="item item-card" style="margin-bottom: 30px;">
	    <div class="img-product-wrapper">
	        <div class="item-card__sticker"></div>
	        <a href="/" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
	            <img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
	        </a>
	        <div class="item-card__info">
	            <div class="item-card__name">
	                <a href="/" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
	            </div>
	            <div class="item-card__prices">
	                <div class="item-card__debet">
	                    <span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
	                </div>
	                <div class="item-card__instalment">
	                    <div class="item-card__prices-title">{!! $item->have !!}</div>

	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endforeach