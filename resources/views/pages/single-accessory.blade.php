@extends('layouts.front')
@section('content')


<section class="slider">
  <div class="owl-carousel">
            @foreach ($slides as $item)
            <div class="item">
              <a href="{!! $item->url !!}">
                <img src="/storage/{!! $item->image !!}" alt="{!! $item->title !!}" class="">
              </a>   
            </div>
            @endforeach
        </div>
</section>
<section class="smartphones">
	<div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
               <strong>{{ $message }}</strong>
            </div>
        @endif
		<div class="pr-single-top">
	<div class="pr-gallery">
		<div id="gallery" class="gallery">
	        <ul class="main" style="margin-bottom:40px;">
	            <li>
	            	<div class="img-wrapper-g">
	            		<img src="/storage/{{ $accessory->image1  }}" alt="">
	            	</div>
	            	
	            </li>
	            <li>
	            	<div class="img-wrapper-g">
	            		<img src="/storage/{{ $accessory->image2  }}" alt="">
					</div>
	            </li>
	            <li>
					<div class="img-wrapper-g">
	            		<img src="/storage/{{ $accessory->image3  }}" alt="">
	            	</div>
	        	</li>
	            <li>
	            	<div class="img-wrapper-g">
	            		<img src="/storage/{{ $accessory->image4  }}" alt="">

					</div>
	            </li>

	        </ul>

	        <ul class="thumb">
	            <li><img src="/storage/{{ $accessory->image1  }}" alt=""></li>
	            <li><img src="/storage/{{ $accessory->image2  }}" alt=""></li>
	            <li><img src="/storage/{{ $accessory->image3  }}" alt=""></li>
	            <li><img src="/storage/{{ $accessory->image4  }}" alt=""></li>

	        </ul>
	    </div>
	</div>
	
<div class="pr-title">
    	<h1 class="item__heading">
            {{ $accessory->title  }}
        </h1>
        <div class="item__price" itemprop="offers" itemscope="">
            <div class="item__price-left-side">
                    <div class="item__price-heading">
                        Цена</div>
                    <div class="item__price-once" content="81570.0" itemprop="price">
                            {{ $accessory->price  }} ₸</div>
                    <span itemprop="priceCurrency" content="KZT"></span>
                    <link itemprop="availability" >
                </div>
                <div class="item__price-right-side">
                    <div class="item__price-heading" style="color: #000;">
                        <br>{{ $accessory->have  }}</div>

                </div>
            <div class="item__benefits">
                <ul class="item__benefits__list">
                    <li class="item__benefits__list-el">
                            <img src="/medias/icon-ok.svg" alt="">Гарантия качества</li>
                    <li class="item__benefits__list-el">
                            <img src="/medias/icon-ok.svg" alt="">Оптимальная цена</li>
                    <li class="item__benefits__list-el">
                            <img src="/medias/icon-ok.svg" alt="">Возврат в течение 14 дней</li>
                    </ul>
            </div>
            <div class="clear"></div>
            <div class="item__buy-button-wrapper">
                <a class="button item__buy-button"  data-toggle="modal" data-target="#exampleModal">
                    Купить</a>
            </div>
        </div>
    </div>

</div>
    <div class="clear"></div>

    	<div class="specifications-list">
	<h2 class="item-content__el-heading">Характеристики&nbsp;{{ $accessory->title  }}</h2>
			{!! $accessory->content  !!}

		</div>
    	






    </div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title text-center" id="exampleModalLabel">&nbsp;{{ $accessory->title  }}</h4>
        <h4 class="modal-price text-center" id="exampleModalLabel2">{{ $accessory->price  }} ₸</h4>
      </div>
      <div class="modal-body" id="contact-content">    
        <form id="contact-form" method="post" action="{{url('/accessories/sendmail')}}" class="contact-form">
            {{csrf_field()}}
            <div class="input-prepend text-center">
                <input id="name" class="name" name="name" type="text" placeholder="Ваше имя">
            </div>
            <p></p> 
            <p> </p>
            <div class="input-prepend text-center">
                <input id="phone" class="phone" name="phone" type="text" placeholder="Ваш телефон" maxlength="17">
            </div>
            <p></p>
            <p></p>
            <center>
                <input  id="submit_btn" class="send-button button item__buy-button" type="submit" name="send" value="Заказать"></input >
            </center>
            <p></p>
        </form>
      </div>

    </div>
  </div>
</div>

</section>


@endsection