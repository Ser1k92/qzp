@extends('layouts.front')
@section('content')

<section class="slider">
  <div class="owl-carousel">
            @foreach ($slides as $item)
            <div class="item">
              <a href="{!! $item->url !!}">
                <img src="/storage/{!! $item->image !!}" alt="{!! $item->title !!}" class="">
              </a>   
            </div>
            @endforeach
        </div>
</section>
<section class="smartphones">
	<div class="content">
		<h2 class="item-teaser__header">Смартфоны</h2>


		<div class="owl-carousel">
            @foreach( $smartphones as $item )
            <div class="item item-card">
            	<div class="img-product-wrapper">
            		<div class="item-card__sticker"></div>
            		<a href="/smartphone/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
            			<img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
            		</a>
            		<div class="item-card__info">
            			<div class="item-card__name">
            				<a href="/smartphone/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
            			</div>
            			<div class="item-card__prices">
            				<div class="item-card__debet">
            					<span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
            				</div>
            				<div class="item-card__instalment">
            					<div class="item-card__prices-title">{!! $item->have !!}</div>

            				</div>
            			</div>
            		</div>
            	</div>
            </div>
            @endforeach
        </div>
		<div class="center">
			<a class="button item__buy-button" href="/smartphones/">
                    Посмотреть все</a>
		</div>
	</div>
</section>
<section class="smartphones">
	<div class="content">
		<h2 class="item-teaser__header">Аксессуары</h2>


		<div class="owl-carousel">
                  @foreach( $accessories as $item )
                        <div class="item item-card">
                              <div class="img-product-wrapper">
                                    <div class="item-card__sticker"></div>
                                    <a href="/accessories/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
                                          <img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
                                    </a>
                                    <div class="item-card__info">
                                          <div class="item-card__name">
                                                <a href="/accessories/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
                                          </div>
                                          <div class="item-card__prices">
                                                <div class="item-card__debet">
                                                      <span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
                                                </div>
                                                <div class="item-card__instalment">
                                                      <div class="item-card__prices-title">{!! $item->have !!}</div>

                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  @endforeach

        </div>
        <div class="center">
			<a class="button item__buy-button" href="/accessories">
                    Посмотреть все</a>
		</div>
	</div>
</section>
<section class="smartphones">
	<div class="content">
		<h2 class="item-teaser__header">Гаджеты</h2>


		<div class="owl-carousel">
                   @foreach( $gadgets as $item )
                        <div class="item item-card">
                              <div class="img-product-wrapper">
                                    <div class="item-card__sticker"></div>
                                    <a href="/gadgets/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
                                          <img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
                                    </a>
                                    <div class="item-card__info">
                                          <div class="item-card__name">
                                                <a href="/gadgets/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
                                          </div>
                                          <div class="item-card__prices">
                                                <div class="item-card__debet">
                                                      <span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
                                                </div>
                                                <div class="item-card__instalment">
                                                      <div class="item-card__prices-title">{!! $item->have !!}</div>

                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  @endforeach
            </div>
        <div class="center">
			<a class="button item__buy-button" href="/gadgets">
                    Посмотреть все</a>
		</div>
	</div>
</section>
<section class="smartphones">
	<div class="content">
		<h2 class="item-teaser__header">Мобильные телефоны</h2>


		<div class="owl-carousel">
                  @foreach( $phones as $item )
                        <div class="item item-card">
                              <div class="img-product-wrapper">
                                    <div class="item-card__sticker"></div>
                                    <a href="/phones/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__image-wrapper ddl_product ddl_product_link">
                                          <img src="/storage/{!! $item->image1 !!}" alt="iphone" class="item-card__image">
                                    </a>
                                    <div class="item-card__info">
                                          <div class="item-card__name">
                                                <a href="/phones/{{ $item->id }}" data-list-id="HP-01-carousel-S-LAST_VIEWED" data-product-id="1003852" class="item-card__name-link ddl_product ddl_product_link">{!! $item->title !!}</a>
                                          </div>
                                          <div class="item-card__prices">
                                                <div class="item-card__debet">
                                                      <span class="item-card__prices-title">Цена: {!! $item->price !!} ₸</span>
                                                </div>
                                                <div class="item-card__instalment">
                                                      <div class="item-card__prices-title">{!! $item->have !!}</div>

                                                </div>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  @endforeach

            </div>
        <div class="center">
			<a class="button item__buy-button" href="/phones">
                    Посмотреть все</a>
		</div>
	</div>
</section>


@endsection