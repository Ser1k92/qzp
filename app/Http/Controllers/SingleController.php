<?php

namespace App\Http\Controllers;

use App\Smartphone;
use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class SingleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $slides = Slide::all();
        $smartphone = Smartphone::where('id', $id)->first();
        return view('pages.single-smartphone', compact('smartphone', 'slides'));
    }

    function send(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'phone'   => 'required'
        ]);

        $data = array(
            'name'      =>  $request->name,
            'phone'   =>   $request->phone
        );

     Mail::to('tolesh0001@gmail.com')->send(new SendMail($data));
     return back()->with('success', 'Спасибо за заказ! Мы обязательно с Вами свяжемся!');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
