<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Slide;
use App\Smartphone;
use Illuminate\Http\Request;

class SmartphoneController extends Controller
{
    
    public function index()
    {
        $brands = Brand::orderBy('title')->get();
        $slides = Slide::all();
        $min_price = Smartphone::min('price');
        $max_price = Smartphone::max('price');
        $smartphones = Smartphone::orderBy('title')->get();
        //dd($min_price);
       return view('pages.smartphones', compact('smartphones', 'slides', 'brands', 'max_price', 'min_price'));
    }

}
