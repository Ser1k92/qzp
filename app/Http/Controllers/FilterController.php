<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Smartphone;
use App\Slide;
use App\Phone;
use App\Gadget;
use App\Accessory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //public $select_brand = [];

    public function index(Request $request)
    {
        
        $select_brand = $request->input('brands');
        $brands = Brand::orderBy('title')->get();
        $slides = Slide::all();
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $smartphones = Smartphone::where(function($query){

        $min_price = Input::has('min_price') ?  Input::get('min_price') : null;
        $max_price = Input::has('max_price') ? Input::get('max_price') : $max_price = null;
        $select_brand = Input::has('brands') ? Input::get('brands') : null;

        if(isset($min_price) && isset($max_price)){

            if(isset($select_brand)){
                foreach ($select_brand as $brand) {
                    $query-> orWhere('price','>=',$min_price);
                    $query-> where('price','<=',$max_price);
                    $query->where('brand','=', $brand);
                }
            }

            $query-> where('price','>=',$min_price);
            $query-> where('price','<=',$max_price);
        }

    })->get();
        //var_dump(array($select_brand));
        return view('pages.smartphones', compact('smartphones','brands', 'slides', 'max_price', 'min_price'));
    }

    public function phone(Request $request)
    {
        
        $select_brand = $request->input('brands');
        $brands = Brand::orderBy('title')->get();
        $slides = Slide::all();
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $phones = Phone::where(function($query){

        $min_price = Input::has('min_price') ?  Input::get('min_price') : null;
        $max_price = Input::has('max_price') ? Input::get('max_price') : $max_price = null;
        $select_brand = Input::has('brands') ? Input::get('brands') : null;

        if(isset($min_price) && isset($max_price)){

            if(isset($select_brand)){
                foreach ($select_brand as $brand) {
                    $query-> orWhere('price','>=',$min_price);
                    $query-> where('price','<=',$max_price);
                    $query->where('brand','=', $brand);
                }
            }

            $query-> where('price','>=',$min_price);
            $query-> where('price','<=',$max_price);
        }

    })->get();
        //var_dump(array($select_brand));
        return view('pages.phones', compact('phones','brands', 'slides', 'max_price', 'min_price'));
    }

    public function gadget(Request $request)
    {
        
        $select_brand = $request->input('brands');
        $brands = Brand::orderBy('title')->get();
        $slides = Slide::all();
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $gadgets = Gadget::where(function($query){

        $min_price = Input::has('min_price') ?  Input::get('min_price') : null;
        $max_price = Input::has('max_price') ? Input::get('max_price') : $max_price = null;
        $select_brand = Input::has('brands') ? Input::get('brands') : null;

        if(isset($min_price) && isset($max_price)){

            if(isset($select_brand)){
                foreach ($select_brand as $brand) {
                    $query-> orWhere('price','>=',$min_price);
                    $query-> where('price','<=',$max_price);
                    $query->where('brand','=', $brand);
                }
            }

            $query-> where('price','>=',$min_price);
            $query-> where('price','<=',$max_price);
        }

    })->get();
        //var_dump(array($select_brand));
        return view('pages.gadgets', compact('gadgets','brands', 'slides', 'max_price', 'min_price'));
    }


    

    public function accessory(Request $request)
    {
        
        $select_brand = $request->input('brands');
        $brands = Brand::orderBy('title')->get();
        $slides = Slide::all();
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $accessories = Accessory::where(function($query){

        $min_price = Input::has('min_price') ?  Input::get('min_price') : null;
        $max_price = Input::has('max_price') ? Input::get('max_price') : $max_price = null;
        $select_brand = Input::has('brands') ? Input::get('brands') : null;

        if(isset($min_price) && isset($max_price)){

            if(isset($select_brand)){
                foreach ($select_brand as $brand) {
                    $query-> orWhere('price','>=',$min_price);
                    $query-> where('price','<=',$max_price);
                    $query->where('brand','=', $brand);
                }
            }

            $query-> where('price','>=',$min_price);
            $query-> where('price','<=',$max_price);
        }

    })->get();
        //var_dump(array($select_brand));
        return view('pages.accessories', compact('accessories','brands', 'slides', 'max_price', 'min_price'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
