<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Slide;
use Illuminate\Http\Request;

use Session;
use Carbon\Carbon;
use File;

class SlidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $slides = Slide::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $slides = Slide::latest()->paginate($perPage);
        }

        return view('admin.slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        
        $slides = new Slide();
        $slides->title=$request->input('title');
        $slides->url=$request->input('url');
            if ($request->hasFile('image')) {
            $docs = $request->file('image'); 
            if($docs->isValid()){ 
                $filename = 'slides/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/slides'; 
                $slides->image=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($slides->save()) {
            Session::flash('flash_message', 'Slide added!');
            return redirect('admin/slides');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $slide = Slide::findOrFail($id);

        return view('admin.slides.show', compact('slide'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $slide = Slide::findOrFail($id);

        return view('admin.slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $slides = Slide::findOrFail($id);
        $slides->title=$request->input('title');
        $slides->url=$request->input('url');
        if ($request->hasFile('image')) {
            File::delete('storage/'.$slides->image1);
            $docs = $request->file('image'); 
            if($docs->isValid()){ 
                $filename = 'slides/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/slides'; 
                $slides->image=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($slides->save()) {

            Session::flash('flash_message', 'Slide updated!');

            return redirect('admin/slides')->with('flash_message', 'Slide updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Slide::destroy($id);

        return redirect('admin/slides')->with('flash_message', 'Slide deleted!');
    }
}
