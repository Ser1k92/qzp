<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Accessory;
use File;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;

class AccessoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $accessories = Accessory::where('title', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('brand', 'LIKE', "%$keyword%")
                ->orWhere('image1', 'LIKE', "%$keyword%")
                ->orWhere('image2', 'LIKE', "%$keyword%")
                ->orWhere('image3', 'LIKE', "%$keyword%")
                ->orWhere('image4', 'LIKE', "%$keyword%")
                ->orWhere('have', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $accessories = Accessory::latest()->paginate($perPage);
        }

        return view('admin.accessories.index', compact('accessories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $brands = Brand::all();
        return view('admin.accessories.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        //$requestData = $request->all();
        $accessories = new Accessory();
        $accessories->title=$request->input('title');
        $accessories->content=$request->input('content');
        $accessories->price=$request->input('price');
        $accessories->brand=$request->input('brand');
        $accessories->have=$request->input('have');

        if ($request->hasFile('image1')) {
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($accessories->save()) {
            Session::flash('flash_message', 'Accessory added!');
            return redirect('admin/accessories');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $accessory = Accessory::findOrFail($id);
        $brands = Brand::all();
        return view('admin.accessories.show', compact('accessory', 'brands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $accessory = Accessory::findOrFail($id);
        $brands = Brand::all();
        return view('admin.accessories.edit', compact('accessory', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        //$requestData = $request->all();
        $accessories = Accessory::findOrFail($id);
        $accessories->title=$request->input('title');
        $accessories->content=$request->input('content');
        $accessories->price=$request->input('price');
        $accessories->brand=$request->input('brand');
        $accessories->have=$request->input('have');
        if ($request->hasFile('image1')) {
            File::delete('storage/'.$accessories->image1);
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            File::delete('storage/'.$accessories->image2);
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            File::delete('storage/'.$accessories->image3);
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            File::delete('storage/'.$accessories->image4);
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'accessories/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/accessories'; 
                $accessories->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }

        if ($accessories->save()) {
            Session::flash('flash_message', 'Accessory updated!');

            return redirect('admin/accessories');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Accessory::destroy($id);

        return redirect('admin/accessories')->with('flash_message', 'Accessory deleted!');
    }
}
