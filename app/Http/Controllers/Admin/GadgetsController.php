<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Gadget;
use File;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;

class GadgetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $gadgets = Gadget::where('title', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('brand', 'LIKE', "%$keyword%")
                ->orWhere('image1', 'LIKE', "%$keyword%")
                ->orWhere('image2', 'LIKE', "%$keyword%")
                ->orWhere('image3', 'LIKE', "%$keyword%")
                ->orWhere('image4', 'LIKE', "%$keyword%")
                ->orWhere('have', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $gadgets = Gadget::latest()->paginate($perPage);
        }

        return view('admin.gadgets.index', compact('gadgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $brands = Brand::all();
        return view('admin.gadgets.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $gadgets = new Gadget();
        $gadgets->title=$request->input('title');
        $gadgets->content=$request->input('content');
        $gadgets->price=$request->input('price');
        $gadgets->brand=$request->input('brand');
        $gadgets->have=$request->input('have');

        if ($request->hasFile('image1')) {
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($gadgets->save()) {
            Session::flash('flash_message', 'Gadget added!');
            return redirect('admin/gadgets');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $gadget = Gadget::findOrFail($id);
        $brands = Brand::all();
        return view('admin.gadgets.show', compact('gadget', 'brands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $gadget = Gadget::findOrFail($id);
        $brands = Brand::all();
        return view('admin.gadgets.edit', compact('gadget', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $gadgets = Gadget::findOrFail($id);
        $gadgets->title=$request->input('title');
        $gadgets->content=$request->input('content');
        $gadgets->price=$request->input('price');
        $gadgets->brand=$request->input('brand');
        $gadgets->have=$request->input('have');
        if ($request->hasFile('image1')) {
            File::delete('storage/'.$gadgets->image1);
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            File::delete('storage/'.$gadgets->image2);
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            File::delete('storage/'.$gadgets->image3);
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            File::delete('storage/'.$gadgets->image4);
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'gadgets/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/gadgets'; 
                $gadgets->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }

        if ($gadgets->save()) {
            Session::flash('flash_message', 'Gadget updated!');

            return redirect('admin/gadgets');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Gadget::destroy($id);

        return redirect('admin/gadgets')->with('flash_message', 'Gadget deleted!');
    }
}
