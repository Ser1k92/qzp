<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Phone;
use File;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;

class PhonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $phones = Phone::where('title', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('brand', 'LIKE', "%$keyword%")
                ->orWhere('image1', 'LIKE', "%$keyword%")
                ->orWhere('image2', 'LIKE', "%$keyword%")
                ->orWhere('image3', 'LIKE', "%$keyword%")
                ->orWhere('image4', 'LIKE', "%$keyword%")
                ->orWhere('have', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $phones = Phone::latest()->paginate($perPage);
        }

        return view('admin.phones.index', compact('phones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $brands = Brand::all();
        return view('admin.phones.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $phones = new Phone();
        $phones->title=$request->input('title');
        $phones->content=$request->input('content');
        $phones->price=$request->input('price');
        $phones->brand=$request->input('brand');
        $phones->have=$request->input('have');

        if ($request->hasFile('image1')) {
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($phones->save()) {
            Session::flash('flash_message', 'Phone added!');
            return redirect('admin/phones');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $phone = Phone::findOrFail($id);
        $brands = Brand::all();
        return view('admin.phones.show', compact('phone', 'brands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $phone = Phone::findOrFail($id);
        $brands = Brand::all();
        return view('admin.phones.edit', compact('phone', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $phones = Phone::findOrFail($id);
        $phones->title=$request->input('title');
        $phones->content=$request->input('content');
        $phones->price=$request->input('price');
        $phones->brand=$request->input('brand');
        $phones->have=$request->input('have');
        if ($request->hasFile('image1')) {
            File::delete('storage/'.$phones->image1);
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            File::delete('storage/'.$phones->image2);
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            File::delete('storage/'.$phones->image3);
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            File::delete('storage/'.$phones->image4);
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'phones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/phones'; 
                $phones->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }

        if ($phones->save()) {
            Session::flash('flash_message', 'Phone updated!');

            return redirect('admin/phones');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Phone::destroy($id);

        return redirect('admin/phones')->with('flash_message', 'Phone deleted!');
    }
}
