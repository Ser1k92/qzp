<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Smartphone;
use File;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;

class SmartphonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $smartphones = Smartphone::where('title', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->orWhere('content', 'LIKE', "%$keyword%")
                ->orWhere('brand', 'LIKE', "%$keyword%")
                ->orWhere('image1', 'LIKE', "%$keyword%")
                ->orWhere('image2', 'LIKE', "%$keyword%")
                ->orWhere('image3', 'LIKE', "%$keyword%")
                ->orWhere('image4', 'LIKE', "%$keyword%")
                ->orWhere('have', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $smartphones = Smartphone::latest()->paginate($perPage);
        }

        return view('admin.smartphones.index', compact('smartphones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $brands = Brand::all();

        return view('admin.smartphones.create', compact( 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $brands = Brand::all();
        $smartphones = new Smartphone();
        $smartphones->title=$request->input('title');
        $smartphones->content=$request->input('content');
        $smartphones->price=$request->input('price');
        $smartphones->brand=$request->input('brand');
        $smartphones->have=$request->input('have');

        if ($request->hasFile('image1')) {
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($smartphones->save()) {
            Session::flash('flash_message', 'Smartphones added!');
            return redirect('admin/smartphones');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $smartphone = Smartphone::findOrFail($id);
        $brands = Brand::all();
        return view('admin.smartphones.show', compact('smartphone', 'brands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $smartphone = Smartphone::findOrFail($id);
        $brands = Brand::all();

        return view('admin.smartphones.edit', compact('smartphone', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        

        //$requestData = $request->all();\
        $smartphones = Smartphone::findOrFail($id);
        $smartphones->title=$request->input('title');
        $smartphones->content=$request->input('content');
        $smartphones->price=$request->input('price');
        $smartphones->brand=$request->input('brand');
        $smartphones->have=$request->input('have');
        if ($request->hasFile('image1')) {
            File::delete('storage/'.$smartphones->image1);
            $docs = $request->file('image1'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image1=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image2')) {
            File::delete('storage/'.$smartphones->image2);
            $docs = $request->file('image2'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image2=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image3')) {
            File::delete('storage/'.$smartphones->image3);
            $docs = $request->file('image3'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image3=$filename;

                $docs->move($path, $filename);  
            } 
        }
        if ($request->hasFile('image4')) {
            File::delete('storage/'.$smartphones->image4);
            $docs = $request->file('image4'); 
            if($docs->isValid()){ 
                $filename = 'smartphones/'.str_random(10).Carbon::now()->format('ymd').'.'.$docs->getClientOriginalExtension(); 
                $path = 'storage/smartphones'; 
                $smartphones->image4=$filename;

                $docs->move($path, $filename);  
            } 
        }

        if ($smartphones->save()) {
            Session::flash('flash_message', 'Smartphone updated!');

            return redirect('admin/smartphones');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Smartphone::destroy($id);

        return redirect('admin/smartphones')->with('flash_message', 'Smartphone deleted!');
    }
}
