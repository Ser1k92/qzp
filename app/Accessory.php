<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accessories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'price', 'content', 'brand', 'image1', 'image2', 'image3', 'image4', 'have'];

    
}
